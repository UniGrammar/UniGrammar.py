import sre_constants as sc
import sre_parse as sp
import typing

from UniGrammarRuntime.backends.regExps.python import PythonRegExpParserFactory, toolGitRepo
from UniGrammarRuntime.IParsingBackend import ToolSpecificGrammarASTWalkStrategy

from ....core.ast import Characters, Comment, Fragmented, Grammar, GrammarMeta, Keywords, MultiLineComment, Name, Productions, Spacer, Tokens
from ....core.ast.base import Node, Ref
from ....core.ast.characters import CharClass, CharClassUnion, CharRange, WellKnownChars
from ....core.ast.prods import Cap, Prefer
from ....core.ast.tokens import Alt, Iter, Lit, Opt, Seq
from ....core.backend.Generator import Generator, GeneratorContext, TranspiledResult
from ....core.backend.Lifter import Lifter, LiftingContext, LiftingVisitor
from .knowledge import anyChar, wellKnownRegExpInvRemapSingle


class PythonRegExpGeneratorContext(GeneratorContext):
	__slots__ = ("sp", "refsPtrs")

	def __init__(self, currentProdName: typing.Optional[str]) -> None:
		self.sp = sp.SubPattern(state)
		self.refsPtrs = []
		super().__init__(currentProdName)


class PythonRegExpGenerator(Generator):
	__slots__ = ()
	META = PythonRegExpParserFactory.FORMAT
	charClassEscaper = None
	stringEscaper = None
	CONTEXT_CLASS = PythonRegExpGeneratorContext

	@classmethod
	def getHeader(cls, obj: Grammar):
		pass

	@classmethod
	def Section(cls, arr: typing.Any, grammar: Grammar, ctx: typing.Any = None):
		res = []
		for obj in arr.children:
			objRes = cls.resolve(obj, grammar, ctx)
			print(obj, objRes)
			res.append(objRes)
		return res

	@classmethod
	def Name(cls, obj: typing.Any, grammar: typing.Optional["Grammar"], ctx: typing.Any = None) -> str:
		print(obj.name, repr(obj.child))
		obj.name
		obj.child
		cls.resolve(obj.child, grammar, ctx)

	@classmethod
	def _Seq(cls, arr: typing.Iterable[Node], grammar: Grammar, ctx: typing.Any = None) -> str:
		for c in arr:
			yield cls.resolve(c, grammar, ctx)

	@classmethod
	def Lit(cls, obj: Lit, grammar: Grammar, ctx: typing.Any = None) -> str:
		return (sc.LITERAL, ord(obj.value))

	@classmethod
	def Iter(cls, obj: Iter, grammar: Grammar, ctx: typing.Any = None) -> str:
		return (sc.MAX_REPEAT, (obj.minCount, sc.MAXREPEAT, [cls.resolve(obj.child, None, ctx)]))

	@classmethod
	def Opt(cls, obj: Opt, grammar: Grammar, ctx: typing.Any = None) -> str:
		return (sc.MAX_REPEAT, (0, 1, [cls.resolve(obj.child, None, ctx)]))

	@classmethod
	def Cap(cls, obj: Cap, grammar: typing.Optional[Grammar], ctx: typing.Any = None) -> str:
		res = cls.resolve(obj.child, grammar, ctx)
		res["cap"] = obj.name
		return res

	@classmethod
	def Spacer(cls, obj: Spacer, grammar: Grammar, ctx: typing.Any = None) -> str:
		pass

	@classmethod
	def Comment(cls, obj: Comment, grammar: Grammar, ctx: typing.Any = None) -> str:
		pass

	@classmethod
	def MultiLineComment(cls, obj: MultiLineComment, grammar: Grammar, ctx: typing.Any = None) -> str:
		pass

	@classmethod
	def Ref(cls, obj: Ref, grammar: Grammar, ctx: typing.Any = None):
		return {"ref": obj.name}

	@classmethod
	def CharClass(cls, obj: CharClass, grammar: Grammar, ctx: typing.Any = None):
		return (sc.IN, [c for c in obj.chars])

	@classmethod
	def WellKnownChars(cls, obj: WellKnownChars, grammar: Grammar, ctx: typing.Any = None) -> str:
		return (sc.CATEGORY, getattr(sc, wellKnownRegExpInvRemapSingle[obj]))

	@classmethod
	def CharClassUnion(cls, obj: CharClassUnion, grammar: Grammar, ctx: typing.Any = None) -> str:
		return (sc.IN, [obj])

	@classmethod
	def CharRange(cls, obj: CharRange, grammar: Grammar, ctx: typing.Any = None) -> str:
		return (sc.RANGE, (obj.start, obj.stop))

	@classmethod
	def _wrapAlts(cls, alts: typing.Iterable[str], grammar: Grammar, ctx: typing.Any = None) -> str:
		return {"alt": list(alts)}

	@classmethod
	def preprocessGrammar(cls, grammar: Grammar, ctx: typing.Any = None) -> None:
		pass

	@classmethod
	def _transpile(cls, grammar: Grammar, ctx: typing.Any = None) -> typing.Iterable[str]:
		cls.embedGrammar(grammar, ctx)
		for secName in cls.getOrder(grammar):
			if not ctx.dict[secName]:
				del ctx.dict[secName]
		return ctx.dict

	@classmethod
	def getOrder(cls, grammar: Grammar, ctx: typing.Any = None) -> typing.Iterable[str]:
		return ("chars", "keywords", "tokens", "fragmented", "prods")

	@classmethod
	def embedGrammar(cls, obj: Grammar, ctx: typing.Any = None) -> None:
		for secName in cls.getOrder(obj):
			sec = getattr(obj, secName)
			secSeq = ctx.dict.get(secName, None)
			if secSeq is None:
				ctx.dict[secName] = secSeq = []
			secSeq.extend(cls.resolve(sec, obj, ctx))
